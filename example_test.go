package goterminal_test

import (
	"fmt"
	"os"

	"gitee.com/shirdonl/goTerminal"
)

func Example() {
	if goterminal.IsTerminal(os.Stdout.Fd()) {
		fmt.Println("Is Terminal")
	} else if goterminal.IsCygwinTerminal(os.Stdout.Fd()) {
		fmt.Println("Is Cygwin/MSYS2 Terminal")
	} else {
		fmt.Println("Is Not Terminal")
	}
}
